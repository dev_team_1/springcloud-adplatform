package cn.dota2info.elk.Service;

import cn.dota2info.elk.dao.ElkSearchDao;
import cn.dota2info.elk.entity.BaseSearchParam;
import cn.dota2info.elk.entity.Book;
import cn.dota2info.elk.entity.MatchSearchParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookSearchService {
    @Autowired
    private ElkSearchDao searchDao;

    public List<Book> termQuery(BaseSearchParam param) throws Exception {
        return searchDao.termQuery("book","doc",param, Book.class);
    }
    public List<Book> matchQuery(MatchSearchParam param) throws Exception {
        return searchDao.matchQuery("book","doc",param,Book.class);
    }

}
